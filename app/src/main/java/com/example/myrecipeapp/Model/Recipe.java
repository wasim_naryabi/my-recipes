package com.example.myrecipeapp.Model;

public class Recipe {

    private String Cost;
    private String Description;
    private String Ingredients;
    private String Path;
    private String People;
    private String Time;
    private String Title;

    public Recipe() {

    }

    public Recipe(String cost, String description, String ingredients, String path, String people, String time, String title) {
        Cost = cost;
        Description = description;
        Ingredients = ingredients;
        Path = path;
        People = people;
        Time = time;
        Title = title;
    }

    public String getCost() {
        return Cost;
    }

    public String getDescription() {
        return Description;
    }

    public String getIngredients() {
        return Ingredients;
    }

    public String getPath() {
        return Path;
    }

    public String getPeople() {
        return People;
    }

    public String getTime() {
        return Time;
    }

    public String getTitle() {
        return Title;
    }
}
