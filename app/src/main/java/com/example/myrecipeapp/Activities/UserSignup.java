package com.example.myrecipeapp.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myrecipeapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class UserSignup extends AppCompatActivity {

    private EditText nameField,
                      emailField,
                       passwordField,
                        conPasswordField;
    private Button signupBtn;
    private TextView loginScreeBtn;
    private String userName,
                    userEmail,
                     userPassword,
                      userConPassword;

    boolean netConnection;
    FirebaseAuth registorAuth;
    DatabaseReference rootRef;

    ProgressBar regProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_signup);
        init();
        checkConnection();
        regProgress.setVisibility(View.GONE);

        registorAuth = FirebaseAuth.getInstance();
        rootRef = FirebaseDatabase.getInstance().getReference();

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                   newUserRegistor();
            }
        });

        loginScreeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginScreen = new Intent(UserSignup.this,UserLogin.class);
                startActivity(loginScreen);
                finish();
            }
        });
    }

    private void newUserRegistor() {
        userName=nameField.getText().toString().trim();
        userEmail=emailField.getText().toString().trim();
        userPassword=passwordField.getText().toString().trim();
        userConPassword=conPasswordField.getText().toString().trim();


        /*// use for keyboard hideing when button click
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);*/

        if(netConnection) {
            if (userName.equals("")) {
                nameField.setError("Name required!");
            } else if (userEmail.equals("")) {
                emailField.setError("Email required!");
            } else if (userPassword.equals("")) {
                passwordField.setError("Password Required!");
            } else if (userPassword.length() < 6) {
                passwordField.setError("At least 6 character password");
            } else if ((!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches())) {
                emailField.setError("Wrong format");
            } else if (!(userPassword.equals(userConPassword))) {
                conPasswordField.setError("password not match");
            } else {
                //regProgress.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                registorAuth.createUserWithEmailAndPassword(userEmail, userPassword)
                        .addOnCompleteListener(UserSignup.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    String currentUserId = registorAuth.getCurrentUser().getUid();
                                    HashMap<String,String> profileMap = new HashMap<>();
                                    profileMap.put("userId",currentUserId);
                                    profileMap.put("name",userName);
                                    profileMap.put("email",userEmail);
                                    rootRef.child("Users").child(currentUserId).setValue(profileMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isComplete()){
                                                regProgress.setVisibility(View.GONE);
                                               // getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                    /*Snackbar snackbar = Snackbar
                                            .make(findViewById(android.R.id.content), "Registration Successful", Snackbar.LENGTH_LONG);
                                    snackbar.show();*/


                                                Toast.makeText(UserSignup.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                                                Intent userData=new Intent(UserSignup.this,MainActivity.class);
                                                userData.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(userData);
                                                finish();
                                            }else{
                                                String massage = task.getException().toString();
                                                regProgress.setVisibility(View.GONE);
                                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                nameField.setText("");
                                                emailField.setText("");
                                                passwordField.setText("");
                                                conPasswordField.setText("");


                                   /* Snackbar snackbar = Snackbar
                                            .make(findViewById(android.R.id.content), massage, Snackbar.LENGTH_LONG);
                                    snackbar.show();
*/
                                                Toast.makeText(UserSignup.this, massage, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });



                                } else {
                                    String massage = task.getException().toString();
                                    regProgress.setVisibility(View.GONE);
                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    nameField.setText("");
                                    emailField.setText("");
                                    passwordField.setText("");
                                    conPasswordField.setText("");


                                   /* Snackbar snackbar = Snackbar
                                            .make(findViewById(android.R.id.content), massage, Snackbar.LENGTH_LONG);
                                    snackbar.show();
*/
                                    Toast.makeText(UserSignup.this, massage, Toast.LENGTH_SHORT).show();
                                }


                            }
                        });
                // Toast.makeText(StdLogin.this, "email is " + sEmail + " password is " + sPassword, Toast.LENGTH_SHORT).show();

            }
        }
        else{
            Toast.makeText(UserSignup.this, "check connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void init() {
        nameField = findViewById(R.id.nameSignUpField);
        emailField = findViewById(R.id.emailSignUpField);
        passwordField = findViewById(R.id.passwordSignUpField);
        conPasswordField = findViewById(R.id.conPasswordSignUpField);
        signupBtn = findViewById(R.id.signUpBtn);
        loginScreeBtn =findViewById(R.id.haveAccountBtn);
        regProgress = findViewById(R.id.progressBar);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserSignup.this,UserLogin.class);
        startActivity(intent);
        finish();
    }

    public void checkConnection(){
        netConnection=false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            netConnection = true;
        }
        else{
            netConnection = false;
        }
    }
}
