package com.example.myrecipeapp.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myrecipeapp.Adopter.RecipeAdopter;
import com.example.myrecipeapp.Model.Recipe;
import com.example.myrecipeapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class DessertsHome extends AppCompatActivity {
    ImageView mBackBtn;
    RecyclerView mRecipesList;
    ArrayList<Recipe> mRecipes;
    RecipeAdopter mRecipeAdopter;
    LinearLayoutManager mLinearLayoutManager;
    //String mImage,mTitle,mTime,mCost,mPeople,mDescription,mIngredients;
    String mGetChild,mRecipeCat;
    Recipe mRecipe;

    private FirebaseAuth mAuth;

    private DatabaseReference mSelectedRecipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desserts_home);

        init();
        toolbarSupport();
        Bundle mExtra = getIntent().getExtras();
        mGetChild=mExtra.getString("Child");
        mRecipeCat="D";
        mRecipeAdopter = new RecipeAdopter(DessertsHome.this, mRecipes,mGetChild,mRecipeCat);
        mSelectedRecipe = FirebaseDatabase.getInstance().getReference().child("Recipe").child("Desserts").child("D").child(mGetChild);

        mSelectedRecipe.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if(dataSnapshot.exists()) {
                    mRecipe = dataSnapshot.getValue(Recipe.class);
                    mRecipes.add(mRecipe);
                    mRecipeAdopter.notifyDataSetChanged();
                    mRecipesList.setAdapter(mRecipeAdopter);



                }else{
                    Toast.makeText(DessertsHome.this, "Sorry some error ", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DessertsHome.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void init() {
        //recipeItem = findViewById(R.id.recipe_Select);
        mRecipesList = findViewById(R.id.item_list);
        mRecipes = new ArrayList<Recipe>();
        mLinearLayoutManager= new LinearLayoutManager(this);
        mRecipesList.setLayoutManager(mLinearLayoutManager);

        mBackBtn = findViewById(R.id.btnBack);

        //mRecipeAdopter= new RecipeAdopter(NonVegeHome.this,mRecipes);
    }

    private void toolbarSupport(){
        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(DessertsHome.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}
