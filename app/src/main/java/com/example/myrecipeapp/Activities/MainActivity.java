package com.example.myrecipeapp.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.myrecipeapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Spinner spinner;
    private DatabaseReference mNonVegeRef,mVegeRef,mDessertsRef;
    private LinearLayout mNonVege,mVege,mDesserts,mFavorite,mAboutApp;
    private FirebaseAuth mAuth;

    private FirebaseUser currentUser;
    private DatabaseReference rootRef;

    boolean netConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        toolbarSupport();

        mAuth = FirebaseAuth.getInstance();
        currentUser =FirebaseAuth.getInstance().getCurrentUser();
        rootRef= FirebaseDatabase.getInstance().getReference();

        mNonVegeRef = FirebaseDatabase.getInstance().getReference().child("Recipe").child("NonVage");
        mVegeRef = FirebaseDatabase.getInstance().getReference().child("Recipe").child("Vage");
        mDessertsRef = FirebaseDatabase.getInstance().getReference().child("Recipe").child("Desserts");

        mNonVege.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
                    View vView = getLayoutInflater().inflate(R.layout.alert_dialog, null);
                    spinner = vView.findViewById(R.id.spinner);
                    mBuilder.setTitle("Select People");
                    mNonVegeRef.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                            if (dataSnapshot.exists()) {
                                final List<String> titleList = new ArrayList<String>();
                                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                    String titlename = dataSnapshot1.getKey();
                                    titleList.add(titlename);

                                }
                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, titleList);
                                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner.setAdapter(arrayAdapter);

                            } else {
                                Toast.makeText(MainActivity.this, "data not exist", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    mBuilder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            String item = spinner.getSelectedItem().toString();
                            if (item.isEmpty()) {
                                Toast.makeText(MainActivity.this, "Plz Select option", Toast.LENGTH_SHORT).show();
                                dialogInterface.dismiss();
                            } else {

                                Intent nonVegeHome = new Intent(MainActivity.this, NonVegeHome.class);
                                nonVegeHome.putExtra("Child", spinner.getSelectedItem().toString());
                                startActivity(nonVegeHome);
                                finish();


                            }


                            //Toast.makeText(MainActivity.this, "Selected "+spinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    mBuilder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    mBuilder.setView(vView);
                    AlertDialog dialog = mBuilder.create();
                    dialog.show();
                }


        });

        mVege.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
                    View vView = getLayoutInflater().inflate(R.layout.alert_dialog, null);
                    spinner = vView.findViewById(R.id.spinner);
                    mBuilder.setTitle("Select People");
                    mVegeRef.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                            if (dataSnapshot.exists()) {
                                final List<String> titleList = new ArrayList<String>();
                                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                    String titlename = dataSnapshot1.getKey();
                                    titleList.add(titlename);

                                }
                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, titleList);
                                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner.setAdapter(arrayAdapter);

                            } else {
                                Toast.makeText(MainActivity.this, "data not exist", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    mBuilder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            String item = spinner.getSelectedItem().toString();
                            if (item.isEmpty()) {
                                Toast.makeText(MainActivity.this, "Plz Select option", Toast.LENGTH_SHORT).show();
                                dialogInterface.dismiss();
                            } else {

                                Intent nonVegeHome = new Intent(MainActivity.this, VegeHome.class);
                                nonVegeHome.putExtra("Child", spinner.getSelectedItem().toString());
                                startActivity(nonVegeHome);
                                finish();

                            }


                            //Toast.makeText(MainActivity.this, "Selected "+spinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    mBuilder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    mBuilder.setView(vView);
                    AlertDialog dialog = mBuilder.create();
                    dialog.show();

            }
        });

        mDesserts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
                    View vView = getLayoutInflater().inflate(R.layout.alert_dialog, null);
                    spinner = vView.findViewById(R.id.spinner);
                    mBuilder.setTitle("Select People");
                    mDessertsRef.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                            if (dataSnapshot.exists()) {
                                final List<String> titleList = new ArrayList<String>();
                                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                    String titlename = dataSnapshot1.getKey();
                                    titleList.add(titlename);

                                }
                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, titleList);
                                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner.setAdapter(arrayAdapter);

                            } else {
                                Toast.makeText(MainActivity.this, "data not exist", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    mBuilder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            String item = spinner.getSelectedItem().toString();

                            if (item.isEmpty()) {
                                Toast.makeText(MainActivity.this, "Plz Select option", Toast.LENGTH_SHORT).show();
                                dialogInterface.dismiss();
                            } else {

                                Intent nonVegeHome = new Intent(MainActivity.this, DessertsHome.class);
                                nonVegeHome.putExtra("Child", spinner.getSelectedItem().toString());
                                startActivity(nonVegeHome);
                                finish();
                            }

                            //Toast.makeText(MainActivity.this, "Selected "+spinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    mBuilder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    mBuilder.setView(vView);
                    AlertDialog dialog = mBuilder.create();
                    dialog.show();
            }
        });

        mFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myFavorite = new Intent(MainActivity.this,MyFavorite.class);
                startActivity(myFavorite);
                finish();
            }
        });

        mAboutApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myFavorite = new Intent(MainActivity.this,AboutApp.class);
                startActivity(myFavorite);
                finish();
            }
        });


    }

    private void init() {

        mNonVege = findViewById(R.id.non_vege);
        mVege = findViewById(R.id.vege);
        mDesserts = findViewById(R.id.desserts);
        mFavorite = findViewById(R.id.favorite);
        mAboutApp = findViewById(R.id.aboutapp);

    }

    private void toolbarSupport(){
        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            case R.id.action_logout:
                mAuth.signOut();
                startActivity(new Intent(MainActivity.this,UserLogin.class));
                finish();
                Toast.makeText(this, "Profile clicked", Toast.LENGTH_SHORT).show();;
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(currentUser == null){
            Toast.makeText(this, "Current User: "+currentUser, Toast.LENGTH_SHORT).show();
            gotologinPage();
        }
        else {
            checkUserExistence();
        }
    }

    @Override
    public void onBackPressed() {
        new androidx.appcompat.app.AlertDialog.Builder(MainActivity.this)
                .setTitle("Exit Application !")
                .setIcon(R.drawable.exit)
                .setMessage("Are you sure to Exit!")
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                }).show();
    }

    private void checkUserExistence() {
        String currentUserId = mAuth.getCurrentUser().getUid();
        rootRef.child("Users").child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if((dataSnapshot.child("name").exists())){
                    Toast.makeText(MainActivity.this, "Welcome", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void gotologinPage() {
        Intent intent = new Intent(MainActivity.this,UserLogin.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void checkConnection(){
        netConnection=false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            netConnection = true;
        }
        else{
            netConnection = false;
        }
    }
}
