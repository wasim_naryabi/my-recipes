package com.example.myrecipeapp.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myrecipeapp.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class UserLogin extends AppCompatActivity{

    private EditText emailField;
    private EditText passwordField;
    private Button loginBtn;
    private TextView signupBtn;
    private String userEmail,
                   userPassword;
    FirebaseAuth registorAuth;

    ProgressBar regProgress;
    boolean netConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);
        init();
        regProgress.setVisibility(View.GONE);
        registorAuth = FirebaseAuth.getInstance();
        checkConnection();
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                userEmail = emailField.getText().toString().trim();
                userPassword = passwordField.getText().toString().trim();


                if(userEmail.isEmpty()){
                    emailField.setError("Email Required");
                }else if(userPassword.isEmpty()){
                    passwordField.setError("password Required");
                }else if (userPassword.length() < 6) {
                    passwordField.setError("At least 6 character password");
                }else if ((!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches())) {
                    emailField.setError("Wrong format");
                }
                else if(netConnection){
                   regProgress.setVisibility(View.VISIBLE);

                    registorAuth.signInWithEmailAndPassword(userEmail,userPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Intent userData = new Intent(UserLogin.this, MainActivity.class);
                                userData.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(userData);
                                finish();
                            } else {
                                String massage = task.getException().toString();
                                regProgress.setVisibility(View.GONE);
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                emailField.setText("");
                                passwordField.setText("");
                                Toast.makeText(UserLogin.this, massage, Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(UserLogin.this, "Please Check Internet connection", Toast.LENGTH_SHORT).show();
                }


            }
        });

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserLogin.this,UserSignup.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(UserLogin.this)
                .setTitle("Exit Application !")
                .setIcon(R.drawable.exit)
                .setMessage("Are you sure to Exit!")
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                }).show();
    }

    private void init() {
        emailField = findViewById(R.id.emailLoginField);
        passwordField = findViewById(R.id.passwordLoginField);
        loginBtn =findViewById(R.id.loginButton);
        signupBtn =findViewById(R.id.newUserBtn);
        regProgress =findViewById(R.id.progressBar);
    }

    public void checkConnection(){
        netConnection=false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            netConnection = true;
        }
        else{
            netConnection = false;
        }
    }

}
