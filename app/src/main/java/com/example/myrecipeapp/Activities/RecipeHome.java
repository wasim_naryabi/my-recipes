package com.example.myrecipeapp.Activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.myrecipeapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class RecipeHome extends AppCompatActivity {

   private TextView mRecipeTitle,
             mRecipeIngredients,
             mRecipeDescription,
             mRecipeTime,
             mRecipePeople,
             mRecipeCost;
   private String mImage,mTitle,mTime,mCost,mPeople,mDescription,mIngredients,mChild,mCat;
   private ImageView mRecipeImage;
   private Bundle mGetValue;

   private ImageView
           mBackBtn,
           mShareBtn,
           mFavBtnSelected,
           mFavBtnNotSelected;

   private StringBuilder item;

    private DatabaseReference rootRef,FavoriteNameRef,mFavoriteRecipe;
    private FirebaseAuth mAuth;

    private String currentUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_home);
        init();
        toolbarSupport();

        mAuth = FirebaseAuth.getInstance();
        currentUserId= mAuth.getCurrentUser().getUid();
        rootRef= FirebaseDatabase.getInstance().getReference();
        FavoriteNameRef = rootRef.child("Favorite");

        mGetValue = getIntent().getExtras();
        mCat = mGetValue.getString("eRecipeCat");
        mChild = mGetValue.getString("eRecipeChild");
        mTitle=mGetValue.getString("eRecipeTitle");
        mCost = mGetValue.getString("eRecipeCost");
        mTime = mGetValue.getString("eRecipeTime");
        mPeople =mGetValue.getString("eRecipePeople");
        mDescription=mGetValue.getString("eRecipeDescription");
        mIngredients = mGetValue.getString("eRecipeIngredients");
        mImage = mGetValue.getString("eRecipeImage");
        mRecipeTitle.setText(mTitle);
        mRecipeCost.setText(mCost);
        mRecipeTime.setText(mTime);
        mRecipePeople.setText(mPeople);
        mRecipeIngredients.setText(mIngredients);
        mRecipeDescription.setText(mDescription);
        Glide.with(RecipeHome.this).load(mGetValue.getString("eRecipeImage")).into(mRecipeImage);

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        mShareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                item.delete(0,item.length());
                item.append("Recipe:\n");
                item.append(mTitle+"\n");

                item.append("Ingredients:\n");
                item.append(mIngredients+"\n");

                item.append("Description:\n");
                item.append(mDescription+"\n");

                item.append("People:\n");
                item.append(mPeople+"\n");

                item.append("Cost:\n");
                item.append(mCost+"\n");

                item.append("Time:\n");
                item.append(mTime+"\n");

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = item.toString();
                String shareSub = "Recipe :"+mTitle ;
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSub);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });

        mFavBtnSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFavBtnNotSelected.setVisibility(View.VISIBLE);
                mFavBtnSelected.setVisibility(View.INVISIBLE);
                removeFavRecipe();
                Toast.makeText(RecipeHome.this, "Selected Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        mFavBtnNotSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFavBtnNotSelected.setVisibility(View.INVISIBLE);
                mFavBtnSelected.setVisibility(View.VISIBLE);
                addFavRecipe();
                Toast.makeText(RecipeHome.this, "Not Selected Clicked", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void addFavRecipe(){

        mFavoriteRecipe = FavoriteNameRef.child(currentUserId).child(mTitle);

        HashMap<String, Object> AddMyRecipe = new HashMap<String, Object>();
        AddMyRecipe.put("Cost", mCost);
        AddMyRecipe.put("Description", mDescription);
        AddMyRecipe.put("Ingredients", mIngredients);
        AddMyRecipe.put("Path", mImage);
        AddMyRecipe.put("People", mPeople);
        AddMyRecipe.put("Time", mTime);
        AddMyRecipe.put("Title", mTitle);
        mFavoriteRecipe.updateChildren(AddMyRecipe);
        checkFavorite();
    }

    private void removeFavRecipe(){
        mFavoriteRecipe = FavoriteNameRef.child(currentUserId).child(mTitle);
        mFavoriteRecipe.removeValue();
        checkFavorite();
    }

    private void init() {
        mRecipeTitle = findViewById(R.id.recipeTitle);
        mRecipeCost  = findViewById(R.id.recipeCost);
        mRecipeDescription = findViewById(R.id.recipeDescription);
        mRecipeIngredients = findViewById(R.id.recipeIngredients);
        mRecipeTime  = findViewById(R.id.recipeTime);
        mRecipePeople = findViewById(R.id.recipePeople);
        mRecipeImage = findViewById(R.id.recipeImage);
        mBackBtn = findViewById(R.id.btnBack);
        mFavBtnSelected = findViewById(R.id.btnFavSelected);
        mFavBtnNotSelected = findViewById(R.id.btnFavNotSelected);
        mShareBtn = findViewById(R.id.btnShare);
        item = new StringBuilder();
    }

    private void checkFavorite(){
        rootRef.child("Favorite").child(currentUserId).child(mTitle).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if((dataSnapshot.exists()) ){
                    String retriveTitle = dataSnapshot.child("Title").getValue().toString();
                    if(retriveTitle.equals(mTitle)){
                        Toast.makeText(RecipeHome.this, retriveTitle, Toast.LENGTH_SHORT).show();
                        mFavBtnSelected.setVisibility(View.VISIBLE);
                    }
                    else {
                        Toast.makeText(RecipeHome.this, retriveTitle, Toast.LENGTH_SHORT).show();
                        mFavBtnNotSelected.setVisibility(View.VISIBLE);
                    }

                }

                else {
                    mFavBtnNotSelected.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(RecipeHome.this, "No Data available", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onBackPressed() {

        goBack();

        }

    private void goBack() {
        String nv = "NV";
        String v = "V";
        String d = "D";

        if(mCat.equals(nv)){
            Intent intent = new Intent(RecipeHome.this,NonVegeHome.class);
            intent.putExtra("Child",mChild);
            startActivity(intent);
            finish();
        }else if(mCat.equals(v)){
            Intent intent = new Intent(RecipeHome.this,VegeHome.class);
            intent.putExtra("Child",mChild);
            startActivity(intent);
            finish();
        }else if(mCat.equals(d)) {
            Intent intent = new Intent(RecipeHome.this, DessertsHome.class);
            intent.putExtra("Child", mChild);
            startActivity(intent);
        }
        else{

            Toast.makeText(this, "Error:"+mCat, Toast.LENGTH_SHORT).show();
        }
    }

    private void toolbarSupport(){
        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
    }

    @Override
    protected void onStart() {
        super.onStart();

        checkFavorite();
    }
}
