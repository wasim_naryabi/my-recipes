package com.example.myrecipeapp.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myrecipeapp.Adopter.FavRecipeAdopter;
import com.example.myrecipeapp.Adopter.RecipeAdopter;
import com.example.myrecipeapp.Model.Recipe;
import com.example.myrecipeapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MyFavorite extends AppCompatActivity {


    private RecyclerView mRecipesList;
    private ArrayList<Recipe> mRecipes;
    private FavRecipeAdopter mRecipeAdopter;
    private LinearLayoutManager mLinearLayoutManager;

    private TextView mUserEmail,mUserName;

    private Recipe mRecipe;
    private String dpName,
            dpEmail,
            currentUserId;

    private FirebaseAuth mAuth;
    private DatabaseReference rootRef;
    private DatabaseReference mSelectedRecipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favorite);
        init();
        toolbarSupport();
        mAuth = FirebaseAuth.getInstance();
        currentUserId= mAuth.getCurrentUser().getUid();
        rootRef= FirebaseDatabase.getInstance().getReference();

        mSelectedRecipe = FirebaseDatabase.getInstance().getReference().child("Favorite").child(currentUserId);

        mSelectedRecipe.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if(dataSnapshot.exists()) {
                    mRecipe = dataSnapshot.getValue(Recipe.class);
                    mRecipes.add(mRecipe);
                    mRecipeAdopter.notifyDataSetChanged();
                    mRecipesList.setAdapter(mRecipeAdopter);

                }else{
                    Toast.makeText(MyFavorite.this, "Sorry some error ", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void init() {

        mRecipesList = findViewById(R.id.item_list);
        mRecipes = new ArrayList<Recipe>();
        mLinearLayoutManager= new LinearLayoutManager(this);
        mRecipesList.setLayoutManager(mLinearLayoutManager);
        mRecipeAdopter = new FavRecipeAdopter(MyFavorite.this, mRecipes);
        mUserName = findViewById(R.id.userName);
        mUserEmail = findViewById(R.id.userEmail);

    }

    private void toolbarSupport(){
        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
    }

    private void getUserdata(){
        rootRef.child("Users").child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if((dataSnapshot.exists()) ){
                    String retriveUserName = dataSnapshot.child("name").getValue().toString();
                    String retriveUserEmail = dataSnapshot.child("email").getValue().toString();

                    mUserName.setText(retriveUserName);
                    mUserEmail.setText(retriveUserEmail);

                }

                else {

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            case R.id.action_logout:
                mAuth.signOut();
                startActivity(new Intent(MyFavorite.this,UserLogin.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getUserdata();
    }

    @Override
    public void onBackPressed() {
       Intent intent = new Intent(MyFavorite.this,MainActivity.class);
       startActivity(intent);
       finish();
    }
}
