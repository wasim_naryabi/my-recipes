package com.example.myrecipeapp.Adopter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.myrecipeapp.Activities.RecipeHome;
import com.example.myrecipeapp.Model.Recipe;
import com.example.myrecipeapp.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

public class FavRecipeAdopter extends RecyclerView.Adapter<FavRecipeAdopter.myViewHolder> {

    Activity activity;
    Context context;
    ArrayList<Recipe> recipes = new ArrayList<Recipe>();
    String mImage,mTitle,mTime,mCost,mPeople,mDescription,mIngredients;

    public FavRecipeAdopter(Context context, ArrayList<Recipe> recipes) {
        this.context = context;
        this.recipes = recipes;

    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        myViewHolder myView = new myViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_item,parent,false));

        return myView;
    }

    @Override
    public void onBindViewHolder(@NonNull myViewHolder holder, int position) {

        activity = (AppCompatActivity)context;
        final Recipe mRecipes = recipes.get(position);
        mImage = mRecipes.getPath();
        mTitle = mRecipes.getTitle();
        mTime = mRecipes.getTime();
        mCost = mRecipes.getCost();
        mDescription=mRecipes.getDescription();
        mPeople = mRecipes.getPeople();
        mIngredients=mRecipes.getIngredients();

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,RecipeHome.class);

                intent.putExtra("eRecipeTitle",mRecipes.getTitle());
                intent.putExtra("eRecipeDescription",mRecipes.getDescription());
                intent.putExtra("eRecipeIngredients",mRecipes.getIngredients());
                intent.putExtra("eRecipeTime",mRecipes.getTime());
                intent.putExtra("eRecipeCost",mRecipes.getCost());
                intent.putExtra("eRecipePeople",mRecipes.getPeople());
                intent.putExtra("eRecipeImage",mRecipes.getPath());
                context.startActivity(intent);
                activity.finish();
            }
        });

        holder.mTitleView.setText(mTitle);
        holder.mTimeView.setText(mTime);
        holder.mCostView.setText(mCost);
        Glide.with(context).load(mImage).into(holder.mRecipeImage);




    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder{

        ImageView mRecipeImage;
        TextView mTitleView,
                 mTimeView,
                 mCostView;


        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            mRecipeImage = (ImageView) itemView.findViewById(R.id.dRecipePic);
            mTitleView = (TextView) itemView.findViewById(R.id.dRecipeTitle);
            mCostView =(TextView)itemView.findViewById(R.id.dRecipeCost);
            mTimeView =(TextView)itemView.findViewById(R.id.dRecipeTime);
        }
    }
}
